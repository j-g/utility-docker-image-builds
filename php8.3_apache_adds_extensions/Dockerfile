# Copyright (c) 2025 James Gardner

FROM php:8.3.17-apache

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

RUN a2enmod headers

RUN docker-php-ext-install bcmath mysqli opcache pdo_mysql

RUN apt-get update && apt-get install -y \
    libbz2-dev \
    libavif15 libavif-dev \
    libfreetype6 libfreetype-dev \
    libjpeg62-turbo libjpeg62-turbo-dev \
    libpng16-16 libpng-dev \
    libwebp7 libwebp-dev \
    libmagickwand-6.q16-6 libmagickwand-6.q16-dev \
    libicu-dev \
    libxslt1.1 libxslt1-dev \
    libzip4 libzip-dev \
    msmtp-mta \
    && docker-php-ext-configure gd --with-avif --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-install bz2 gd intl xsl zip \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && apt-get remove -y \
    libbz2-dev \
    libavif-dev libfreetype-dev libjpeg62-turbo-dev libpng-dev libwebp-dev \
    libmagickwand-6.q16-dev \
    libicu-dev \
    libxslt1-dev \
    libzip-dev \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*
